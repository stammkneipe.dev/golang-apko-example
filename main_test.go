package main

import (
	"bytes"
	"testing"
)

func TestMain(t *testing.T) {
	var buf bytes.Buffer
	out = &buf

	main()

	expected := "Hello, World!\n"
	got := buf.String()

	if got != expected {
		t.Errorf("Expected %q, but got %q", expected, got)
	}
}
