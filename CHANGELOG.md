## [1.0.4](https://gitlab.com/stammkneipe.dev/golang-apko-example/compare/1.0.3...1.0.4) (2023-12-26)


### Bug Fixes

* ci ([9a28331](https://gitlab.com/stammkneipe.dev/golang-apko-example/commit/9a283313bb3c0360ab9e6d3462d4bf0457e2ca6e))

## [1.0.3](https://gitlab.com/stammkneipe.dev/golang-apko-example/compare/1.0.2...1.0.3) (2023-12-26)


### Bug Fixes

* Update melange.yml with next release version ([5f219cb](https://gitlab.com/stammkneipe.dev/golang-apko-example/commit/5f219cb8bee6e03a7b80f45c2a15aaf0a034b5f9))

## [1.0.2](https://gitlab.com/stammkneipe.dev/golang-apko-example/compare/1.0.1...1.0.2) (2023-12-26)


### Bug Fixes

* Update .releaserc.yml to include version in melange.yml ([8c8d87f](https://gitlab.com/stammkneipe.dev/golang-apko-example/commit/8c8d87f6a831caa52a5e0fac4c66e9821a19118a))

## [1.0.1](https://gitlab.com/stammkneipe.dev/golang-apko-example/compare/1.0.0...1.0.1) (2023-12-26)


### Bug Fixes

* ci ([8bb9fa0](https://gitlab.com/stammkneipe.dev/golang-apko-example/commit/8bb9fa0cbcb314172ffc8fc5e7b284ed224be13e))
* conditional statement in .gitlab-ci.yml ([48374db](https://gitlab.com/stammkneipe.dev/golang-apko-example/commit/48374db37099da21466858ad54b6b488cd99f0a4))
* pipeline condition in .gitlab-ci.yml and update version in .releaserc.yml ([d495961](https://gitlab.com/stammkneipe.dev/golang-apko-example/commit/d495961c4d88ceadceb0f7bbaf8aecb00bd3c033))
* Update GitLab CI rules for scheduled pipelines ([6e8d39d](https://gitlab.com/stammkneipe.dev/golang-apko-example/commit/6e8d39d496c078d3124581ddf23021128888a919))

# 1.0.0 (2023-12-26)


### Bug Fixes

* melange ([bbf52ae](https://gitlab.com/stammkneipe.dev/golang-apko-example/commit/bbf52ae05cffd547179f2a24aaa3422839e0b229))
* melange kubernetes config ([77da0cf](https://gitlab.com/stammkneipe.dev/golang-apko-example/commit/77da0cf537ab3d3384005967309ffc4b11cea116))
* use gitlab registry ([7e79646](https://gitlab.com/stammkneipe.dev/golang-apko-example/commit/7e79646b3211d463e93032dc3d713c889c63691e))


### Features

* init project ([6a01635](https://gitlab.com/stammkneipe.dev/golang-apko-example/commit/6a016352753e35dda692dce96512466ce6fe4656))
* remove yq ([405dc08](https://gitlab.com/stammkneipe.dev/golang-apko-example/commit/405dc08e99728860439385ac7c4e74bab8ea7840))
